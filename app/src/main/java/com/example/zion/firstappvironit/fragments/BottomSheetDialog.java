package com.example.zion.firstappvironit.fragments;

import static android.app.Activity.RESULT_OK;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import com.example.zion.firstappvironit.R;
import java.io.ByteArrayOutputStream;

public class BottomSheetDialog extends BottomSheetDialogFragment {

  private static final int STORAGE_REQUEST_GET_SINGLE_FILE = 1;
  private static final int CAMERA_REQUEST_GET_SINGLE_FILE = 2;


  private BottomSheetListener mListener;

  @androidx.annotation.Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater,
      @androidx.annotation.Nullable ViewGroup container,
      @androidx.annotation.Nullable Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.bottom_sheet_layout, container, false);

    ImageButton cameraButton = v.findViewById(R.id.button_camera);
    ImageButton storageButton = v.findViewById(R.id.button_storage);

    storageButton.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Select Picture"),
            STORAGE_REQUEST_GET_SINGLE_FILE);

      }
    });

    cameraButton.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(takePictureIntent, CAMERA_REQUEST_GET_SINGLE_FILE);
      }
    });

    return v;
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    try {
      if (resultCode == RESULT_OK) {
        if (requestCode == STORAGE_REQUEST_GET_SINGLE_FILE) {

          Uri imageUri = data.getData();

          Bitmap selectedImage = MediaStore.Images.Media
              .getBitmap(getActivity().getContentResolver(), imageUri);

          mListener.onButtonClicked(convertBitmapToByteArray(selectedImage));
          dismiss();

        }
        if (requestCode == CAMERA_REQUEST_GET_SINGLE_FILE) {
          Bundle extras = data.getExtras();
          Bitmap imageCameraBitmap = (Bitmap) extras.get("data");

          mListener.onButtonClicked(convertBitmapToByteArray(imageCameraBitmap));
          dismiss();
        }
      }
    } catch (Exception e) {
      Log.e("FileSelectorActivity", "File select error", e);
    }
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);

    try {
      mListener = (BottomSheetListener) context;
    } catch (ClassCastException e) {
      throw new ClassCastException(context.toString()
          + " must implement BottomSheetListener");
    }
  }

  protected byte[] convertBitmapToByteArray(Bitmap bitmap) {
    ByteArrayOutputStream bos = new ByteArrayOutputStream();
    bitmap.compress(CompressFormat.JPEG, 50
        , bos);
    byte[] bArray = bos.toByteArray();
    return bArray;
  }

  public interface BottomSheetListener {

    void onButtonClicked(byte[] byteArray);
  }


}
