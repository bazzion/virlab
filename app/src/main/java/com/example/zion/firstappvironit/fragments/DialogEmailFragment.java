package com.example.zion.firstappvironit.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.core.content.FileProvider;
import androidx.appcompat.app.AlertDialog;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import com.example.zion.firstappvironit.BuildConfig;
import com.example.zion.firstappvironit.R;
import com.example.zion.firstappvironit.custom.CustomEditTextAddStudent;
import com.example.zion.firstappvironit.model.App;
import com.example.zion.firstappvironit.model.VironitStudent;
import com.example.zion.firstappvironit.model.VironitStudentsDao;
import com.example.zion.firstappvironit.model.VironitStudentsDataBase;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

public class DialogEmailFragment extends DialogFragment {

  public static final String ERROR = "error";
  public static final String INCORRECT_EMAIL = "incorrectEmail";
  private final static String FILE_NAME = "students.txt";
  CustomEditTextAddStudent emailEditText;


  VironitStudentsDataBase dataBase = App.getInstance().getDatabase();
  VironitStudentsDao vironitStudentsDao = dataBase.vironitStudentsDao();
  List<VironitStudent> mVironitStudents;

  File studentsFile;


  @NonNull
  @Override
  public Dialog onCreateDialog(
      @Nullable Bundle savedInstanceState) {

    AlertDialog.Builder sendAlertDialog = new AlertDialog.Builder(Objects
        .requireNonNull(getContext()));

    View view = LayoutInflater.from(getContext())
        .inflate(R.layout.custom_dialog_email, null, false);

    emailEditText = view.findViewById(R.id.sendAsFileCustomEditText);

    sendAlertDialog.setTitle("Send as file")
        .setView(view)
        .setPositiveButton("Send", new OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {

          }

        })
        .setNegativeButton("Cancel", new OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
            dismiss();
          }
        });

    AlertDialog dialog = sendAlertDialog.create();
    dialog.show();
    Button positiveButton = dialog.getButton(Dialog.BUTTON_POSITIVE);
    positiveButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        if (checkingStateAndCorrectEmail()) {
          sendAndSaveFile();
          dismiss();
        }

      }
    });

    return dialog;
  }


  public void sendAndSaveFile() {

    saveFile();
    sendFile();

  }

  public void saveFile() {

    mVironitStudents = vironitStudentsDao.getAll();

    try {

      File folder = Environment
          .getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
      studentsFile = new File(folder, FILE_NAME);
      FileWriter fileWriter = new FileWriter(studentsFile);
      BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
      bufferedWriter.write(mVironitStudents.toString());
      bufferedWriter.close();

    } catch (IOException e) {
      e.printStackTrace();
    }

  }

  public void sendFile() {

    Uri studentsInformationFileUri = FileProvider
        .getUriForFile(Objects.requireNonNull(getContext()),
            BuildConfig.APPLICATION_ID + ".provider", studentsFile);

    Intent sendFileIntent = new Intent(Intent.ACTION_SEND);
    sendFileIntent.setType("text/plain");
    sendFileIntent
        .putExtra(Intent.EXTRA_EMAIL,
            new String[]{Objects.requireNonNull(emailEditText.getText()).toString()});
    sendFileIntent.putExtra(Intent.EXTRA_SUBJECT, "Students report");
    sendFileIntent.putExtra(Intent.EXTRA_STREAM, studentsInformationFileUri);
    if (sendFileIntent.resolveActivity(Objects.requireNonNull(getActivity()).getPackageManager())
        != null) {
      startActivity(sendFileIntent);
    }
  }

  private boolean checkingStateAndCorrectEmail() {

    if (Objects.requireNonNull(emailEditText.getText()).toString().equals("")) {
      emailEditText.setBorderState(ERROR);
      return false;
    }
    if (!validEmail(emailEditText.getText().toString())) {
      emailEditText.setBorderState(INCORRECT_EMAIL);
      return false;
    }

    return true;
  }

  private boolean validEmail(String email) {
    Pattern pattern = Patterns.EMAIL_ADDRESS;
    return pattern.matcher(email).matches();
  }

}
