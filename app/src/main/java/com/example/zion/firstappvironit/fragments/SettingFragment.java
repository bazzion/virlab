package com.example.zion.firstappvironit.fragments;

import android.content.Context;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import com.example.zion.firstappvironit.R;
import com.example.zion.firstappvironit.broadcast.CheckBroadcast;
import com.example.zion.firstappvironit.broadcast.CheckBroadcast.snackBroadcast;
import java.util.Objects;

public class SettingFragment extends Fragment {

  private final static String SETTINGS = "settings";
  private final static String PREF_ROUND = "round";
  private final static String PREF_SQUARE = "square";

  CheckBroadcast mCheckBroadcast = new CheckBroadcast();

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_setting, container, false);

    Context context = getActivity();
    SharedPreferences settings = context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);
    final SharedPreferences.Editor prefEditor = settings.edit();

    RadioGroup switchPhotos = view.findViewById(R.id.switchPhotos);
    RadioButton setSquarePhotos = view.findViewById(R.id.setSquarePhotos);
    RadioButton setRoundPhotos = view.findViewById(R.id.setRoundPhotos);

      setRoundPhotos.setChecked(settings.getBoolean(PREF_ROUND, true));
      setSquarePhotos.setChecked(settings.getBoolean(PREF_SQUARE, false));



    switchPhotos.setOnCheckedChangeListener(new OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
          case R.id.setSquarePhotos:
            prefEditor.putBoolean(PREF_SQUARE, true);
            prefEditor.putBoolean(PREF_ROUND, false);
            prefEditor.apply();
            break;
          case R.id.setRoundPhotos:
            prefEditor.putBoolean(PREF_ROUND, true);
            prefEditor.putBoolean(PREF_SQUARE, false);
            prefEditor.apply();
            break;
          default:
            break;
        }
      }
    });



    return view;

  }

  @Override
  public void onResume() {
    super.onResume();
    regBroadcastAndSnack();

  }

  @Override
  public void onPause() {
    super.onPause();
    Objects.requireNonNull(getActivity()).unregisterReceiver(mCheckBroadcast);
  }


  private void regBroadcastAndSnack() {
    mCheckBroadcast.setSnackBroadcast(new snackBroadcast() {
      @Override
      public void setSnackOnActivity(String wifiStatus) {
        final Snackbar snackbar = Snackbar
            .make(Objects.requireNonNull(getView()), wifiStatus, Snackbar.LENGTH_SHORT);
        snackbar.setAction("close", new OnClickListener() {
          @Override
          public void onClick(View v) {
            snackbar.dismiss();
          }
        });
        snackbar.setActionTextColor(Color.parseColor("#fa3e3e"));
        snackbar.show();
      }
    });

    Objects.requireNonNull(getActivity()).registerReceiver(mCheckBroadcast,
        new IntentFilter(WifiManager.SUPPLICANT_CONNECTION_CHANGE_ACTION));
  }

}
