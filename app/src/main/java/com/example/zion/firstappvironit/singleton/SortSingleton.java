package com.example.zion.firstappvironit.singleton;


public class SortSingleton {


  private SortSingleton() {
  }

  public static SortSingleton getInstance() {
    return SingletonHolder.ourInstance;
  }

  public void Sort(sortCallback mSortCallback) {

    int[] array;
    array = new int[10000];
    for (int i = 0; i < array.length; i++) {
      array[i] = ((int) (Math.random()));
    }

    for (int i = array.length - 1; i > 0; i--) {
      for (int j = 0; j < i; j++) {
        if (array[j] > array[j + 1]) {
          int tmp = array[j];
          array[j] = array[j + 1];
          array[j + 1] = tmp;
        }
      }

    }
    if (mSortCallback != null) {
      mSortCallback.endSorting();
    }

  }

  public interface sortCallback {

    void endSorting();
  }


  private static class SingletonHolder {

    private static final SortSingleton ourInstance = new SortSingleton();
  }
}