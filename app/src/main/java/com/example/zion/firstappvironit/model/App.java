package com.example.zion.firstappvironit.model;

import android.app.Application;
import androidx.room.Room;
import com.crashlytics.android.Crashlytics;
import com.squareup.leakcanary.LeakCanary;
import io.fabric.sdk.android.Fabric;

public class App extends Application {

  public static App instance;

  private VironitStudentsDataBase database;

  public static App getInstance() {
    return instance;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    Fabric.with(this, new Crashlytics());
    if (LeakCanary.isInAnalyzerProcess(this)) {
      // This process is dedicated to LeakCanary for heap analysis.
      // You should not init your app in this process.
      return;
    }
    LeakCanary.install(this);
    instance = this;
    database = Room.databaseBuilder(this, VironitStudentsDataBase.class, "database")
        .allowMainThreadQueries()
        .build();

  }

  public VironitStudentsDataBase getDatabase() {
    return database;
  }

}
