package com.example.zion.firstappvironit.model;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = VironitStudent.class, version = 1)
public abstract class VironitStudentsDataBase extends RoomDatabase {

  public abstract VironitStudentsDao vironitStudentsDao();

}
