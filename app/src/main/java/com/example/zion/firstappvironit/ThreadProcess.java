package com.example.zion.firstappvironit;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import androidx.appcompat.app.AppCompatActivity;
import com.example.zion.firstappvironit.service.IntentServiceSort;
import com.example.zion.firstappvironit.singleton.SortSingleton;
import com.example.zion.firstappvironit.singleton.SortSingleton.sortCallback;
import com.example.zion.firstappvironit.broadcast.CheckBroadcast;
import com.example.zion.firstappvironit.broadcast.CheckBroadcast.snackBroadcast;
import com.google.android.material.snackbar.Snackbar;


public class ThreadProcess extends AppCompatActivity implements SortSingleton.sortCallback {

  private final static int ANIM_DURATION = 1000;
  CheckBroadcast mCheckBroadcast = new CheckBroadcast();
  ImageView circleForAnim;
  Button uiButton;
  Button threadButton;
  Button handlerThreadButton;
  Button intentServiceButton;
  Button asyncTaskButton;
  Context context;
  private sortHandlerThread mSortHandlerThread;
//  private SortBroadcastReceiver mSortBroadcastReceiver;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_thread_process);

    circleForAnim = findViewById(R.id.circleForAnim);
    uiButton = findViewById(R.id.uiThreadButton);
    threadButton = findViewById(R.id.justThreadButton);
    handlerThreadButton = findViewById(R.id.handlerThreadButton);
    intentServiceButton = findViewById(R.id.intentServiceButton);
    asyncTaskButton = findViewById(R.id.asyncTaskAndroid);

    context = this;

    repeatAnim();

    uiButton.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        SortSingleton.getInstance().Sort((sortCallback) context);

      }
    });

    threadButton.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(final View v) {
        justThreadSort();
      }
    });

    handlerThreadButton.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {

        handlerThreadSort();
      }
    });

    intentServiceButton.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {

        Intent sortIntentService = new Intent(context, IntentServiceSort.class);
        startService(sortIntentService);

        IntentFilter sortFilter = new IntentFilter(IntentServiceSort.ACTION_MYINTENTSERVICE);
        sortFilter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(mCheckBroadcast, sortFilter);


      }
    });

    asyncTaskButton.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        new sortAsyncTask().execute();
      }
    });

  }


  @Override
  protected void onResume() {
    super.onResume();
    regBroadcastAndSnack();
  }

  @Override
  protected void onPause() {
    super.onPause();
    unregisterReceiver(mCheckBroadcast);
  }

  @Override
  protected void onDestroy() {
    if (mSortHandlerThread != null) {
      mSortHandlerThread.quit();
    }
    super.onDestroy();

  }

  @Override
  public void finish() {
    super.finish();
    overridePendingTransition(R.anim.anim_open_activity, R.anim.anim_close_activity);
  }


  private void handlerThreadSort() {
    mSortHandlerThread = new sortHandlerThread("sortHandlerThread");
    Runnable task = new Runnable() {
      @Override
      public void run() {
        SortSingleton.getInstance().Sort((sortCallback) context);
      }
    };

    mSortHandlerThread.start();
    mSortHandlerThread.prepareHandler();
    mSortHandlerThread.postTask(task);
  }

  private void justThreadSort() {
    new Thread(new Runnable() {
      @Override
      public void run() {
        SortSingleton.getInstance().Sort((sortCallback) context);

      }
    }).start();

  }


  private void repeatAnim() {
    ObjectAnimator repeatX = ObjectAnimator.ofFloat(circleForAnim, "scaleX", 0f, 1f);
    repeatX.setDuration(ANIM_DURATION);
    repeatX.setRepeatCount(ObjectAnimator.INFINITE);
    repeatX.setRepeatMode(ObjectAnimator.REVERSE);
    ObjectAnimator repeatY = ObjectAnimator.ofFloat(circleForAnim, "scaleY", 0f, 1f);
    repeatY.setDuration(ANIM_DURATION);
    repeatY.setRepeatCount(ObjectAnimator.INFINITE);
    repeatY.setRepeatMode(ObjectAnimator.REVERSE);
    repeatX.start();
    repeatY.start();
  }


  private void regBroadcastAndSnack() {
    mCheckBroadcast.setSnackBroadcast(new snackBroadcast() {
      @Override
      public void setSnackOnActivity(String wifiStatus) {
        showSnack(wifiStatus);
      }
    });

    registerReceiver(mCheckBroadcast,
        new IntentFilter(WifiManager.SUPPLICANT_CONNECTION_CHANGE_ACTION));
  }

  @Override
  public void endSorting() {
    showSnack(null);
  }


  private void showSnack(String stats) {

    final Snackbar snackbar;

    if (stats == null) {
      snackbar = Snackbar.make((findViewById(R.id.threadMainLayout)), "Sort done", Snackbar.LENGTH_SHORT);
    } else {
      snackbar = Snackbar.make((findViewById(R.id.threadMainLayout)), stats, Snackbar.LENGTH_SHORT);
    }
    snackbar.setAction("close", new OnClickListener() {
      @Override
      public void onClick(View v) {
        snackbar.dismiss();
      }
    });
    snackbar.setActionTextColor(Color.parseColor("#fa3e3e"));
    snackbar.show();
  }

  public class sortHandlerThread extends HandlerThread {

    private Handler mSortHandler;

    public sortHandlerThread(String name) {
      super(name);
    }

    public void postTask(Runnable task) {
      mSortHandler.post(task);
    }

    public void prepareHandler() {
      mSortHandler = new Handler(getLooper());
    }
  }


  public class sortAsyncTask extends AsyncTask<Void, Void, Void> {

    @Override
    protected Void doInBackground(Void... voids) {

      SortSingleton.getInstance().Sort((sortCallback) context);

      return null;
    }


  }
}
