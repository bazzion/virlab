package com.example.zion.firstappvironit.myFirebase;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import com.example.zion.firstappvironit.MainActivity;
import com.example.zion.firstappvironit.R;
import com.example.zion.firstappvironit.broadcast.CheckBroadcast;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import java.util.Objects;
import java.util.Random;


public class MyFirebaseMessagingService extends FirebaseMessagingService {


  public static final String ADMIN_CHANNEL_ID = "admin_channel";
  public static final String IMAGE_KEY_NOTIFICATION = "image";
  public static final String FIRST_NAME_KEY_NOTIFICATION = "first_name";
  public static final String LAST_NAME_KEY_NOTIFICATION = "last_name";
  public static final String ADDITIONAL_INFO_KEY_NOTIFICATION = "additional_info";
  public static final String PHONE_NUMBER_KEY_NOTIFICATION = "phone_number";
  public static final String IQ_STATS_KEY_NOTIFICATION = "iq_stats";

  public static final String ACTION_SEND_BROADCAST = "com.example.zion.firstappvironit.myfirebase.RESPONSE";


  private NotificationManager notificationManager;

  @Override
  public void onMessageReceived(RemoteMessage remoteMessage) {


    Intent notificationIntent = new Intent(this, MainActivity.class);

    notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

    final PendingIntent pendingIntentNotification = PendingIntent.getActivity(this, 0, notificationIntent,
        PendingIntent.FLAG_ONE_SHOT);

    int notificationId = new Random().nextInt(60000);

    Intent likeIntent = new Intent(this, CheckBroadcast.class);
    likeIntent.setAction(ACTION_SEND_BROADCAST);
    likeIntent
        .putExtra(IMAGE_KEY_NOTIFICATION, remoteMessage.getData().get(IMAGE_KEY_NOTIFICATION));
    likeIntent.putExtra(FIRST_NAME_KEY_NOTIFICATION,
        remoteMessage.getData().get(FIRST_NAME_KEY_NOTIFICATION));
    likeIntent.putExtra(LAST_NAME_KEY_NOTIFICATION,
        remoteMessage.getData().get(LAST_NAME_KEY_NOTIFICATION));
    likeIntent.putExtra(ADDITIONAL_INFO_KEY_NOTIFICATION,
        remoteMessage.getData().get(ADDITIONAL_INFO_KEY_NOTIFICATION));
    likeIntent.putExtra(PHONE_NUMBER_KEY_NOTIFICATION,
        remoteMessage.getData().get(PHONE_NUMBER_KEY_NOTIFICATION));
    likeIntent.putExtra(IQ_STATS_KEY_NOTIFICATION,
         remoteMessage.getData().get(IQ_STATS_KEY_NOTIFICATION));
    PendingIntent pendingIntentLike = PendingIntent.getBroadcast(this, 1, likeIntent, 0);

    notificationManager =
        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
      setupChannels();
    }

    NotificationCompat.Builder notificationBuilder =
        new NotificationCompat.Builder(this, ADMIN_CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_camera)
            .setContentTitle(Objects.requireNonNull(remoteMessage.getNotification()).getTitle())
            .setContentText(remoteMessage.getNotification().getBody())
            .setAutoCancel(true)
            .setDefaults(Notification.DEFAULT_ALL)
            .setContentIntent(pendingIntentNotification)
        .addAction(R.drawable.floating_plus, "ADD", pendingIntentLike);

    notificationManager.notify(notificationId, notificationBuilder.build());

  }


  @RequiresApi(api = Build.VERSION_CODES.O)
  private void setupChannels() {
    CharSequence adminChannelName = "Global channel";
    String adminChannelDescription = "Notifications sent from the app admin";

    NotificationChannel adminChannel;
    adminChannel = new NotificationChannel(ADMIN_CHANNEL_ID, adminChannelName,
        NotificationManager.IMPORTANCE_LOW);
    adminChannel.setDescription(adminChannelDescription);
    adminChannel.enableLights(true);
    adminChannel.setLightColor(Color.RED);
    adminChannel.enableVibration(true);
    if (notificationManager != null) {
      notificationManager.createNotificationChannel(adminChannel);
    }
  }
}