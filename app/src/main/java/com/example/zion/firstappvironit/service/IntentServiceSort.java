package com.example.zion.firstappvironit.service;

import android.app.IntentService;
import android.content.Intent;
import androidx.annotation.Nullable;
import com.example.zion.firstappvironit.singleton.SortSingleton;

public class IntentServiceSort extends IntentService implements SortSingleton.sortCallback {


  public static final String ACTION_MYINTENTSERVICE = " com.example.zion.firstappvironit.service.RESPONSE";
  private final static String name = "intent_service_sort";

  public IntentServiceSort() {
    super(name);
  }

  @Override
  protected void onHandleIntent(@Nullable Intent intent) {

    SortSingleton.getInstance().Sort(null);
    sortResponse();

  }

  private void sortResponse() {

    Intent responseIntent = new Intent();
    responseIntent.setAction(ACTION_MYINTENTSERVICE);
    responseIntent.addCategory(Intent.CATEGORY_DEFAULT);
    sendBroadcast(responseIntent);

  }

  @Override
  public void endSorting() {

  }
}
