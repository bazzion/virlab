package com.example.zion.firstappvironit.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import androidx.annotation.StringDef;
import androidx.core.content.ContextCompat;
import androidx.appcompat.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.example.zion.firstappvironit.R;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class CustomEditTextAddStudent extends AppCompatEditText {


  public static final String OK = "ok";
  public static final String ERROR = "error";
  public static final String NO_ACTION = "noAction";
  public static final String INCORRECT_EMAIL = "incorrectEmail";
  public static final String POPUP_FULL_FIELD_TEXT = "full field";
  public static final String POPUP_INCORRECT_EMAIL_TEXT = "incorrect email";
  @CustomEditTextAddStudentDef
  String borderState;
  Drawable mDrawable;

  PopupWindow customPopup;
  View popupLayoutView;
  TextView textViewPopup;


  boolean okFlag = false;
  boolean errorFlag = false;


  public CustomEditTextAddStudent(Context context) {
    super(context);
    init(context, null);

  }

  public CustomEditTextAddStudent(Context context, AttributeSet attrs) {
    super(context, attrs);
    init(context, attrs);
  }

  public CustomEditTextAddStudent(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init(context, attrs);
  }

  public void init(Context context, AttributeSet attrs) {
    TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomEditTextAddStudent);
    mDrawable = ContextCompat.getDrawable(getContext(), R.drawable.red_border);
    borderState = a.getString(R.styleable.CustomEditTextAddStudent_error) != null ?
        a.getString(R.styleable.CustomEditTextAddStudent_error) :
        CustomEditTextAddStudent.NO_ACTION;

    customPopup = new PopupWindow();

    popupLayoutView = inflate(context, R.layout.custom_popup_layout, null);
    textViewPopup = popupLayoutView.findViewById(R.id.tooltip_text);

    setImageOfStatusEditText();

    addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

        if (s.length() == 0) {
          borderState = ERROR;
          errorFlag = true;
        } else {
          borderState = OK;
          okFlag = true;
        }

        if (errorFlag && okFlag) {
          borderState = NO_ACTION;
          errorFlag = false;
          okFlag = false;

        }
        setImageOfStatusEditText();

      }

      @Override
      public void afterTextChanged(Editable s) {

      }
    });
  }

  private void setImageOfStatusEditText() {
    Drawable drawable = null;

    switch (borderState) {
      case OK:
        drawable = getResources().getDrawable(R.drawable.ic_done_green_24dp);
        setBackground(null);
        break;
      case ERROR:
        drawable = getResources().getDrawable(R.drawable.ic_error_red_24dp);
        setBackground(mDrawable);
        textViewPopup.setText(POPUP_FULL_FIELD_TEXT);
        showPopup(this);
        break;
      case INCORRECT_EMAIL:
        drawable = getResources().getDrawable(R.drawable.ic_error_red_24dp);
        setBackground(mDrawable);
        textViewPopup.setText(POPUP_INCORRECT_EMAIL_TEXT);
        showPopup(this);

      case NO_ACTION:
        setCompoundDrawables(null, null, null, null);
        break;
    }
    setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null);

  }

  public void showPopup(View v) {

    customPopup.setContentView(popupLayoutView);
    customPopup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
    customPopup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
    customPopup.setFocusable(false);
    customPopup.setOutsideTouchable(true);
    customPopup.showAsDropDown(v, 0, 0, Gravity.CENTER);

  }


  public void setBorderState(String borderState) {
    this.borderState = borderState;
    setImageOfStatusEditText();
  }


  @Retention(RetentionPolicy.SOURCE)
  @StringDef({OK, ERROR, NO_ACTION, INCORRECT_EMAIL})
  private @interface CustomEditTextAddStudentDef {

  }
}
