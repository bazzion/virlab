package com.example.zion.firstappvironit;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.example.zion.firstappvironit.fragments.SettingFragment;

public class FragmentSettingsActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_settings_fragment);

    FragmentManager fragmentManager = getSupportFragmentManager();
    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

    SettingFragment settingFragment = new SettingFragment();


    fragmentTransaction.add(R.id.frameLayoutFragment, settingFragment);

    fragmentTransaction.commit();
  }

  @Override
  public void finish() {
    super.finish();
    overridePendingTransition(R.anim.anim_open_activity, R.anim.anim_close_activity);
  }
}
