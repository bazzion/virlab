package com.example.zion.firstappvironit;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import com.example.zion.firstappvironit.model.VironitStudent;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.zion.firstappvironit.broadcast.CheckBroadcast;
import com.example.zion.firstappvironit.broadcast.CheckBroadcast.snackBroadcast;
import com.example.zion.firstappvironit.custom.CustomCircleDiagram;
import com.example.zion.firstappvironit.model.App;
import com.example.zion.firstappvironit.model.VironitStudentsDao;
import com.example.zion.firstappvironit.model.VironitStudentsDataBase;

public class BigInfoActivity extends AppCompatActivity {

  private final static int TIMEANIMATION = 1000;

  private final static String SETTINGS = "settings";
  private final static String PREF_ROUND = "round";
  private final static String PREF_SQUARE = "square";

  ImageView imageView;
  TextView firstNameTV;
  TextView lastNameTV;
  TextView additionalInfoTV;
  TextView phoneNumberTV;
  Button deleteButton;
  CustomCircleDiagram mCustomCircleDiagram;

  VironitStudentsDataBase dataBase = App.getInstance().getDatabase();
  VironitStudentsDao vironitStudentsDao = dataBase.vironitStudentsDao();

  CheckBroadcast mCheckBroadcast = new CheckBroadcast();

  VironitStudent studentList;

  public VironitStudent getStudentList() {
    return studentList;
  }

  public void setStudentList(VironitStudent studentList) {
    this.studentList = studentList;
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_big_info);

    imageView = findViewById(R.id.imageBigInfo);
    firstNameTV = findViewById(R.id.firstNameBigInfo);
    lastNameTV = findViewById(R.id.lastNameBigInfo);
    additionalInfoTV = findViewById(R.id.additionalInfoBigInfo);
    phoneNumberTV = findViewById(R.id.phoneNumberBigInfo);
    deleteButton = findViewById(R.id.deleteButton);

    Intent intent = getIntent();

    final long id = intent.getExtras().getLong("ID");

    studentList = vironitStudentsDao.getById(id);

    setStudentList(studentList);

    mCustomCircleDiagram = findViewById(R.id.circle_diagram);
    mCustomCircleDiagram.setIntegerForStats(studentList.getStudentProgress());

    firstNameTV.setText(studentList.getName());
    lastNameTV.setText(studentList.getLastName());
    additionalInfoTV.setText(studentList.getAdditionalInfo());
    phoneNumberTV.setText(studentList.getPhoneNumber());

    startTextAnimation();
    startImageAnimation();

    deleteButton.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {

        deleteStudents(id);

      }
    });
  }


  public void deleteStudents(long id) {
    VironitStudentsDataBase dataBase = App.getInstance().getDatabase();
    VironitStudentsDao vironitStudentsDao = dataBase.vironitStudentsDao();
    VironitStudent vironitStudent = new VironitStudent();
    vironitStudent.setId(id);
    vironitStudentsDao.delete(vironitStudent);

    finish();
  }

  private void startTextAnimation() {
    AnimatorSet textTranslation = new AnimatorSet();
    textTranslation.playTogether(ObjectAnimator.ofFloat(firstNameTV, "translationX", 1000f, 0f),
        ObjectAnimator.ofFloat(lastNameTV, "translationX", 1000f, 0f),
        ObjectAnimator.ofFloat(additionalInfoTV, "translationX", 1000f, 0f),
        ObjectAnimator.ofFloat(phoneNumberTV, "translationX", 1000f, 0f));
    textTranslation.setDuration(TIMEANIMATION);
    textTranslation.start();
  }


  private void startImageAnimation() {
    AnimatorSet imageVisible = new AnimatorSet();
    imageVisible.playTogether(ObjectAnimator.ofFloat(mCustomCircleDiagram, "scaleX", 0f, 1f),
        ObjectAnimator.ofFloat(mCustomCircleDiagram, "scaleY", 0f, 1f),
        ObjectAnimator.ofFloat(imageView, "scaleX", 0f, 1f),
        ObjectAnimator.ofFloat(imageView, "scaleY", 0f, 1f));
    imageVisible.setDuration(TIMEANIMATION);
    imageVisible.start();

  }


  public void drawImage(VironitStudent studentList) {
    SharedPreferences settings = getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);

    Bitmap bm = null;

    if (studentList.getImage() != null) {
      byte[] byteArray = studentList.getImage();
      bm = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
    }

    if (settings.getBoolean(PREF_ROUND, true)) {
      if (studentList.getImage() != null) {
        Glide.with(this)
            .load(bm)
            .apply(RequestOptions.circleCropTransform())
            .into(imageView);
      } else {
        Glide.with(this)
            .load(R.drawable.ic_camera)
            .apply(RequestOptions.circleCropTransform())
            .into(imageView);
      }
    } else if (settings.getBoolean(PREF_SQUARE, false)) {
      if (studentList.getImage() != null) {
        Glide.with(this)
            .load(bm)
            .apply(RequestOptions.centerCropTransform())
            .into(imageView);
      } else {
        Glide.with(this)
            .load(R.drawable.ic_camera)
            .apply(RequestOptions.centerCropTransform())
            .into(imageView);
      }
    }
  }

  @Override
  protected void onResume() {
    super.onResume();
    drawImage(getStudentList());
    regBroadcastAndSnack();
  }

  @Override
  protected void onPause() {
    super.onPause();
    unregisterReceiver(mCheckBroadcast);
  }


  @Override
  public void finish() {
    super.finish();
    overridePendingTransition(R.anim.anim_open_activity, R.anim.anim_close_activity);
  }


  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu, menu);
    MenuItem addStudent = menu.findItem(R.id.addAddNewStudent);
    addStudent.setVisible(false);
    MenuItem send = menu.findItem(R.id.sendAsFile);
    send.setVisible(false);
    MenuItem threadProcess = menu.findItem(R.id.threadProcess);
    threadProcess.setVisible(false);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();

    if (id == R.id.settings) {
      Intent startSettingsActivityFragment = new Intent(this, FragmentSettingsActivity.class);
      startActivity(startSettingsActivityFragment);
    }

    return super.onOptionsItemSelected(item);
  }


  private void regBroadcastAndSnack() {
    mCheckBroadcast.setSnackBroadcast(new snackBroadcast() {
      @Override
      public void setSnackOnActivity(String wifiStatus) {
        final Snackbar snackbar = Snackbar
            .make((findViewById(R.id.bigInfoMainLayout)), wifiStatus, Snackbar.LENGTH_SHORT);
        snackbar.setAction("close", new OnClickListener() {
          @Override
          public void onClick(View v) {
            snackbar.dismiss();
          }
        });
        snackbar.setActionTextColor(Color.parseColor("#fa3e3e"));
        snackbar.show();
      }
    });

    registerReceiver(mCheckBroadcast,
        new IntentFilter(WifiManager.SUPPLICANT_CONNECTION_CHANGE_ACTION));
  }

}
