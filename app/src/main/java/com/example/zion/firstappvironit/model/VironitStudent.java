package com.example.zion.firstappvironit.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class VironitStudent {

  @PrimaryKey(autoGenerate = true)
  private long id;

  @ColumnInfo(typeAffinity = ColumnInfo.BLOB)
  private byte[] image;

  private String name;
  private String lastName;
  private String additionalInfo;
  private String phoneNumber;
  private int studentProgress;

  public int getStudentProgress() {
    return studentProgress;
  }

  public void setStudentProgress(int studentProgress) {
    this.studentProgress = studentProgress;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }


  public byte[] getImage() {
    return image;
  }

  public void setImage(byte[] image) {
    this.image = image;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getAdditionalInfo() {
    return additionalInfo;
  }

  public void setAdditionalInfo(String additionalInfo) {
    this.additionalInfo = additionalInfo;
  }

  @NonNull
  @Override
  public String toString() {
    return
        "\n" + "ID: " + id + "\n" +
            "Name: " + name + "\n" +
            "LastName: " + lastName + "\n" +
            "AdditionalInfo: " + additionalInfo + "\n" +
            "PhoneNumber: " + phoneNumber + "\n" +
            "StudentProgress: " + studentProgress +
            "\n";
  }
}
