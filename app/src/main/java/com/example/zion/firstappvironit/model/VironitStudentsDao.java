package com.example.zion.firstappvironit.model;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import java.util.List;

@Dao
public interface VironitStudentsDao {

  @Query("SELECT * FROM  VironitStudent")
  List<VironitStudent> getAll();

  @Query("SELECT * FROM VironitStudent WHERE id = :id")
  VironitStudent getById(long id);

  @Query("DELETE FROM VironitStudent WHERE id = :id")
  void deleteById(long id);

  @Insert
  void insert(VironitStudent vironitStudent);

  @Update
  void update(VironitStudent vironitStudent);

  @Delete
  void delete(VironitStudent vironitStudent);

}
