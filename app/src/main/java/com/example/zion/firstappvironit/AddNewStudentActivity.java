package com.example.zion.firstappvironit;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import com.example.zion.firstappvironit.model.VironitStudent;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.zion.firstappvironit.broadcast.CheckBroadcast;
import com.example.zion.firstappvironit.broadcast.CheckBroadcast.snackBroadcast;
import com.example.zion.firstappvironit.custom.CustomEditTextAddStudent;
import com.example.zion.firstappvironit.fragments.BottomSheetDialog;
import com.example.zion.firstappvironit.model.App;
import com.example.zion.firstappvironit.model.VironitStudentsDao;
import com.example.zion.firstappvironit.model.VironitStudentsDataBase;
import java.util.Objects;

public class AddNewStudentActivity extends AppCompatActivity implements
    BottomSheetDialog.BottomSheetListener {


  public static final String ERROR = "error";

  private final static String SETTINGS = "settings";
  private final static String PREF_ROUND = "round";
  private final static String PREF_SQUARE = "square";

  ImageView imageViewStudentImage;
  CustomEditTextAddStudent editTextStudentName;
  CustomEditTextAddStudent editTextStudentLastName;
  CustomEditTextAddStudent editTextStudentAdditionalInfo;
  CustomEditTextAddStudent editTextStudentPhoneNumber;
  Toolbar toolbar;
  SeekBar statsSeekBar;
  TextView changeValueSeekBar;
  VironitStudentsDataBase dataBase = App.getInstance().getDatabase();
  VironitStudentsDao vironitStudentsDao = dataBase.vironitStudentsDao();
  VironitStudent newVironitStudent = new VironitStudent();
  byte[] globalBitmap;

  CheckBroadcast mCheckBroadcast = new CheckBroadcast();

  SharedPreferences settings;

  public void setGlobalBitmap(byte[] globalBitmap) {
    this.globalBitmap = globalBitmap;
  }


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_add_new_student);

    imageViewStudentImage = findViewById(R.id.addNewStudentImage);
    editTextStudentName = findViewById(R.id.addNewStudentName);
    editTextStudentLastName = findViewById(R.id.addNewStudentLastName);
    editTextStudentAdditionalInfo = findViewById(R.id.addNewStudentAdditionalInfo);
    editTextStudentPhoneNumber = findViewById(R.id.addNewStudentPhone);
    statsSeekBar = findViewById(R.id.statsSeekBar);
    changeValueSeekBar = findViewById(R.id.changeValueSeekBar);

    changeValueSeekBar.setText(String.valueOf(0));

    statsSeekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
      @Override
      public void onProgressChanged(SeekBar statsSeekBar, int progress, boolean fromUser) {

        changeValueSeekBar.setText(String.valueOf(progress));

      }

      @Override
      public void onStartTrackingTouch(SeekBar statsSeekBar) {

      }

      @Override
      public void onStopTrackingTouch(SeekBar statsSeekBar) {

      }
    });

    toolbar = findViewById(R.id.addNewStudentToolbar);
    setSupportActionBar(toolbar);
    Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowHomeEnabled(true);

    imageViewStudentImage.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog();
        bottomSheetDialog.show(getSupportFragmentManager(), "bottomSheet");
      }
    });
  }

  @Override
  protected void onResume() {
    super.onResume();
    regBroadcastAndSnack();
    drawImage();
  }

  @Override
  protected void onPause() {
    super.onPause();
    unregisterReceiver(mCheckBroadcast);
  }


  @Override
  public void onButtonClicked(byte[] byteArray) {
    setGlobalBitmap(byteArray);
  }


  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu, menu);
    MenuItem send = menu.findItem(R.id.sendAsFile);
    send.setVisible(false);
    MenuItem threadProcess = menu.findItem(R.id.threadProcess);
    threadProcess.setVisible(false);
    return true;
  }


  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();

    if (id == R.id.addAddNewStudent) {
      if (checkingState()) {
        addingStudent();
        onBackPressed();
        return true;
      }
    }

    if (id == R.id.settings) {
      Intent startSettingsActivityFragment = new Intent(this, FragmentSettingsActivity.class);
      startActivity(startSettingsActivityFragment);
    }

    return super.onOptionsItemSelected(item);
  }


  @Override
  public boolean onSupportNavigateUp() {
    onBackPressed();
    return true;
  }

  @Override
  public void finish() {
    super.finish();
    overridePendingTransition(R.anim.anim_open_activity, R.anim.anim_close_activity);
  }


  private void drawImage() {
    settings = getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);
    if (globalBitmap != null) {
      Bitmap bitmapImageForGlide = byteConverter(globalBitmap);
      if (settings.getBoolean(PREF_ROUND, true)) {
        Glide.with(this)
            .load(bitmapImageForGlide)
            .apply(RequestOptions.circleCropTransform())
            .into(imageViewStudentImage);
      } else if (settings.getBoolean(PREF_SQUARE, false)) {
        Glide.with(this)
            .load(bitmapImageForGlide)
            .apply(RequestOptions.centerCropTransform())
            .into(imageViewStudentImage);
      }
    } else if (globalBitmap == null) {
      if (settings.getBoolean(PREF_ROUND, true)) {
        Glide.with(this)
            .load(R.drawable.ic_camera)
            .apply(RequestOptions.circleCropTransform())
            .into(imageViewStudentImage);
      } else if (settings.getBoolean(PREF_SQUARE, false)) {
        Glide.with(this)
            .load(R.drawable.ic_camera)
            .apply(RequestOptions.centerCropTransform())
            .into(imageViewStudentImage);
      }
    }
  }


  public void addingStudent() {

    if (globalBitmap != null) {
      newVironitStudent.setImage(globalBitmap);
    }
    newVironitStudent.setName(editTextStudentName.getText().toString());
    newVironitStudent.setLastName(editTextStudentLastName.getText().toString());
    newVironitStudent.setAdditionalInfo(editTextStudentAdditionalInfo.getText().toString());
    newVironitStudent.setPhoneNumber(editTextStudentPhoneNumber.getText().toString());
    newVironitStudent.setStudentProgress(statsSeekBar.getProgress());
    vironitStudentsDao.insert(newVironitStudent);
  }

  private boolean checkingState() {
    if (editTextStudentName.getText().toString().equals("")) {
      editTextStudentName.setBorderState(ERROR);
      return false;
    }
    if (editTextStudentLastName.getText().toString().equals("")) {
      editTextStudentLastName.setBorderState(ERROR);
      return false;
    }
    if (editTextStudentAdditionalInfo.getText().toString().equals("")) {
      editTextStudentAdditionalInfo.setBorderState(ERROR);
      return false;
    }
    if (editTextStudentPhoneNumber.getText().toString().equals("")) {
      editTextStudentPhoneNumber.setBorderState(ERROR);
      return false;
    }
    return true;
  }

  public Bitmap byteConverter(byte[] byteArray) {
    Bitmap bm = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
    return bm;
  }

  private void regBroadcastAndSnack() {
    mCheckBroadcast.setSnackBroadcast(new snackBroadcast() {
      @Override
      public void setSnackOnActivity(String wifiStatus) {
        final Snackbar snackbar = Snackbar
            .make((findViewById(R.id.addMainLayout)), wifiStatus, Snackbar.LENGTH_SHORT);
        snackbar.setAction("close", new OnClickListener() {
          @Override
          public void onClick(View v) {
            snackbar.dismiss();
          }
        });
        snackbar.setActionTextColor(Color.parseColor("#fa3e3e"));
        snackbar.show();
      }
    });

    registerReceiver(mCheckBroadcast,
        new IntentFilter(WifiManager.SUPPLICANT_CONNECTION_CHANGE_ACTION));
  }


}
