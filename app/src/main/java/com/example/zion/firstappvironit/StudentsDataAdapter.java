package com.example.zion.firstappvironit;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.zion.firstappvironit.model.VironitStudent;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import java.util.ArrayList;
import java.util.List;

public class StudentsDataAdapter extends RecyclerView.Adapter<StudentsDataAdapter.ViewHolder> {


  private final static String SETTINGS = "settings";
  private final static String PREF_ROUND = "round";
  private final static String PREF_SQUARE = "square";
  private final OnItemClickListener listener;
  private final PhoneClickListener phoneListener;
  private Context context;
  private LayoutInflater inflater;
  private List<VironitStudent> mVironitStudents;
  private VironitStudent returnVironitStudent = new VironitStudent();

  StudentsDataAdapter(Context context, OnItemClickListener listener,
      PhoneClickListener phoneListener) {
    this.inflater = LayoutInflater.from(context);
    this.context = context;
    this.listener = listener;
    this.phoneListener = phoneListener;
  }

  @NonNull
  @Override
  public StudentsDataAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
    View view = inflater.inflate(R.layout.list_students, viewGroup, false);
    return new ViewHolder(view, listener, phoneListener);
  }

  @Override
  public void onBindViewHolder(@NonNull StudentsDataAdapter.ViewHolder viewHolder, int position) {
    VironitStudent students = mVironitStudents.get(position);
    viewHolder.bind(students);
  }

  @Override
  public int getItemCount() {
    return mVironitStudents.size();
  }

  public void deleteStudent(int position) {
    returnVironitStudent = mVironitStudents.get(position);
    mVironitStudents.remove(position);
    notifyItemRemoved(position);
  }

  public void returnStudentAfterDeleting(int position) {
    mVironitStudents.add(position, returnVironitStudent);
    notifyItemInserted(position);
  }

  public List<VironitStudent> getVironitStudents() {
    return mVironitStudents;
  }

  public void setVironitStudents(
      List<VironitStudent> vironitStudents) {
//    if (mVironitStudents != null) {
//      this.mVironitStudents.clear();
//      this.mVironitStudents.addAll(mVironitStudents);
//    } else {
    this.mVironitStudents = new ArrayList<>(vironitStudents);
//    }
  }

  public interface OnItemClickListener {

    void onItemClick(VironitStudent students);
  }

  public interface PhoneClickListener {

    void onPhoneClick(VironitStudent students);
  }

  public class ViewHolder extends RecyclerView.ViewHolder {

    final ImageView imageView;
    final TextView phoneNumberText, firstNameText, lastNameText, additionalInfoText;
    final FloatingActionButton phoneFloatingButton;
    final ProgressBar valueProgressBar;
    private VironitStudent mStudent;

    ViewHolder(View view, final OnItemClickListener listener,
        final PhoneClickListener phoneListener) {
      super(view);

      imageView = view.findViewById(R.id.imageStudents);
      firstNameText = view.findViewById(R.id.firstNameTextView);
      lastNameText = view.findViewById(R.id.lastNameTextView);
      additionalInfoText = view.findViewById(R.id.additionalInfoTextView);
      phoneNumberText = view.findViewById(R.id.phoneNumberTextView);
      phoneFloatingButton = view.findViewById(R.id.phoneFloatingButton);
      valueProgressBar = view.findViewById(R.id.valueProgressBar);

      Drawable draw = ContextCompat.getDrawable(context, R.drawable.custom_progress_bar);
      valueProgressBar.setProgressDrawable(draw);

      phoneFloatingButton.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
          phoneListener.onPhoneClick(mStudent);
        }
      });

      view.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
          listener.onItemClick(mStudent);
        }
      });
    }

    public void bind(final VironitStudent students) {
      mStudent = students;

      Bitmap bm = null;

      SharedPreferences settings = context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);

      if (students.getImage() != null) {
        byte[] byteArray = students.getImage();
        bm = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
      }

      if (settings.getBoolean(PREF_ROUND, true)) {
        if (students.getImage() != null) {
          Glide.with(context)
              .load(bm)
              .apply(RequestOptions.circleCropTransform())
              .into(imageView);
        } else {
          Glide.with(context)
              .load(R.drawable.ic_camera)
              .apply(RequestOptions.circleCropTransform())
              .into(imageView);
        }
      } else if (settings.getBoolean(PREF_SQUARE, false)) {
        if (students.getImage() != null) {
          Glide.with(context)
              .load(bm)
              .apply(RequestOptions.centerCropTransform())
              .into(imageView);
        } else {
          Glide.with(context)
              .load(R.drawable.ic_camera)
              .apply(RequestOptions.centerCropTransform())
              .into(imageView);
        }
      }

      firstNameText.setText(students.getName());
      lastNameText.setText(students.getLastName());
      additionalInfoText.setText(students.getAdditionalInfo());
      phoneNumberText.setText(students.getPhoneNumber());
      valueProgressBar.setProgress(students.getStudentProgress());
    }
  }
}




