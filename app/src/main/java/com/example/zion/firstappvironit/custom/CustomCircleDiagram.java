package com.example.zion.firstappvironit.custom;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;



public class CustomCircleDiagram extends View {


  Paint backgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
  Paint piePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
  public final static int backgroundColor =  Color.GRAY;
  public final static int pieStatsColor = Color.GREEN;
  public int statsSize = 0;
  public static final int startAngle = 270;



  public CustomCircleDiagram(Context context, AttributeSet attrs) {
    super(context, attrs);


  }


  @Override
  protected void onDraw(Canvas canvas) {
    super.onDraw(canvas);


    drawBackgroundCircle(canvas);
    drawPieStats(canvas);

  }

  public void drawBackgroundCircle(Canvas canvas) {
    backgroundPaint.setColor(backgroundColor);
    canvas.drawCircle(getWidth()/2f, getHeight()/2f, getHeight()/2f, backgroundPaint);
  }

  public void drawPieStats(Canvas canvas) {
    piePaint.setDither(true);
    piePaint.setStyle(Paint.Style.FILL);
    int top = 0;
    int left = 0;
    int endBottom = getHeight();
    int endRight = endBottom;
    float pieSize = (float) statsSize / 100f * 360f;
    RectF rectF = new RectF(left, top, endRight, endBottom);
    piePaint.setColor(pieStatsColor);
    canvas.drawArc(rectF, startAngle, pieSize, true, piePaint);
  }

  public void setIntegerForStats(int statsSize){
    this.statsSize = statsSize;
    invalidate();
  }


}
