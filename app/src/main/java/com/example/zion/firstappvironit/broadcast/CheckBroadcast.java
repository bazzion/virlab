package com.example.zion.firstappvironit.broadcast;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import com.example.zion.firstappvironit.model.App;
import com.example.zion.firstappvironit.model.VironitStudent;
import com.example.zion.firstappvironit.model.VironitStudentsDao;
import com.example.zion.firstappvironit.model.VironitStudentsDataBase;
import com.example.zion.firstappvironit.myFirebase.MyFirebaseMessagingService;
import com.example.zion.firstappvironit.service.IntentServiceSort;
import java.util.Objects;

public class CheckBroadcast extends BroadcastReceiver {

  public static final String WIFI_CONNECTED = "Wi-fi ON";
  public static final String WIFI_DISCONNECTED = "Wi-fi OFF";
  public static final String SORT_DONE = "Sort done";
  public snackBroadcast mSnackBroadcast;


  public void setSnackBroadcast(
      snackBroadcast snackBroadcast) {
    mSnackBroadcast = snackBroadcast;
  }

  @Override
  public void onReceive(Context context, Intent intent) {

    final String action = intent.getAction();
    assert action != null;
    if (action.equals(WifiManager.SUPPLICANT_CONNECTION_CHANGE_ACTION)) {
      if (intent.getBooleanExtra(WifiManager.EXTRA_SUPPLICANT_CONNECTED, false)) {
        mSnackBroadcast.setSnackOnActivity(WIFI_CONNECTED);
      } else {
        mSnackBroadcast.setSnackOnActivity(WIFI_DISCONNECTED);
      }
    }

    if(action.equals(IntentServiceSort.ACTION_MYINTENTSERVICE)) {
      mSnackBroadcast.setSnackOnActivity(SORT_DONE);
    }


    if (action.equals(MyFirebaseMessagingService.ACTION_SEND_BROADCAST)) {

      VironitStudentsDataBase dataBase = App.getInstance().getDatabase();
      VironitStudentsDao vironitStudentsDao = dataBase.vironitStudentsDao();

      VironitStudent VS = new VironitStudent();

      VS.setName(intent.getExtras().getString(MyFirebaseMessagingService.FIRST_NAME_KEY_NOTIFICATION));
      VS.setLastName(intent.getExtras().getString(MyFirebaseMessagingService.LAST_NAME_KEY_NOTIFICATION));
      VS.setAdditionalInfo(intent.getExtras().getString(MyFirebaseMessagingService.ADDITIONAL_INFO_KEY_NOTIFICATION));
      VS.setPhoneNumber(intent.getExtras().getString(MyFirebaseMessagingService.PHONE_NUMBER_KEY_NOTIFICATION));
      VS.setStudentProgress(Integer.parseInt(Objects.requireNonNull(
          intent.getExtras().getString(MyFirebaseMessagingService.IQ_STATS_KEY_NOTIFICATION))));

      vironitStudentsDao.insert(VS);

    }

  }

  public interface snackBroadcast {

    void setSnackOnActivity(String wifiStatus);
  }

}
