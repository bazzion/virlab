package com.example.zion.firstappvironit;

import android.Manifest;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import com.example.zion.firstappvironit.StudentsDataAdapter.OnItemClickListener;
import com.example.zion.firstappvironit.StudentsDataAdapter.PhoneClickListener;
import com.example.zion.firstappvironit.broadcast.CheckBroadcast;
import com.example.zion.firstappvironit.broadcast.CheckBroadcast.snackBroadcast;
import com.example.zion.firstappvironit.fragments.DialogEmailFragment;
import com.example.zion.firstappvironit.model.App;
import com.example.zion.firstappvironit.model.VironitStudent;
import com.example.zion.firstappvironit.model.VironitStudentsDao;
import com.example.zion.firstappvironit.model.VironitStudentsDataBase;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import java.util.List;

public class MainActivity extends AppCompatActivity {


  VironitStudentsDataBase dataBase = App.getInstance().getDatabase();
  VironitStudentsDao vironitStudentsDao = dataBase.vironitStudentsDao();
  FloatingActionButton floatingActionButton;
  RecyclerView recyclerView;
  StudentsDataAdapter adapter;

  private static final String TAG = "MainActivity";

  CheckBroadcast mCheckBroadcast = new CheckBroadcast();
  List<VironitStudent> studentsList;


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && this.checkSelfPermission(
        Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
      this.requestPermissions(new String[]{
          Manifest.permission.READ_EXTERNAL_STORAGE,
          Manifest.permission.WRITE_EXTERNAL_STORAGE,
      }, 23);
    }

    getFirebaseToken();

    getView();

    floatingActionButton = findViewById(R.id.floating_action_button);
    floatingActionButton.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        startAddNewStudentActivity();
      }
    });
  }

  @Override
  protected void onResume() {
    super.onResume();

    createView();
    setUpItemTouchHelper(recyclerView);
    regBroadcastAndSnack();


  }

  @Override
  protected void onPause() {
    super.onPause();
    unregisterReceiver(mCheckBroadcast);
  }


  private void getFirebaseToken() {
    FirebaseInstanceId.getInstance().getInstanceId()
        .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
          @Override
          public void onComplete(@NonNull Task<InstanceIdResult> task) {
            if (!task.isSuccessful()) {
              return;
            }

            String token = task.getResult().getToken();
            String msg = getString(R.string.fcm_token, token);
            Log.d(TAG, msg);

          }
        });
  }

  private void getView() {
    recyclerView = findViewById(R.id.list);
    recyclerView.setLayoutManager(new LinearLayoutManager(this));
    adapter = new StudentsDataAdapter(this, new OnItemClickListener() {
      @Override
      public void onItemClick(VironitStudent students) {
        startBigInfoActivity(students);
      }
    }, new PhoneClickListener() {
      @Override
      public void onPhoneClick(VironitStudent students) {
        if (!students.getPhoneNumber().equals("")) {
          startCall(students);
        }
      }
    });
    recyclerView.setAdapter(adapter);
  }


  private void createView() {
    studentsList = vironitStudentsDao.getAll();
    adapter.notifyDataSetChanged();
    adapter.setVironitStudents(studentsList);


  }

  public void startCall(VironitStudent students) {
    Intent phoneIntent = new Intent(Intent.ACTION_DIAL,
        Uri.parse("tel:" + students.getPhoneNumber()));
    startActivity(phoneIntent);
  }

  public void startBigInfoActivity(VironitStudent students) {
    long id = students.getId();
    Intent bigInfoIntent = new Intent(this, BigInfoActivity.class);
    bigInfoIntent.putExtra("ID", id);
    startActivity(bigInfoIntent);
    overridePendingTransition(R.anim.anim_open_activity, R.anim.anim_close_activity);
  }

  public void startAddNewStudentActivity() {
    Intent addNewStudentActivity = new Intent(this, AddNewStudentActivity.class);
    startActivity(addNewStudentActivity);
    overridePendingTransition(R.anim.anim_open_activity, R.anim.anim_close_activity);
  }

  private void setUpItemTouchHelper(RecyclerView recyclerView) {

    ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0,
        ItemTouchHelper.LEFT) {
      @Override
      public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull ViewHolder viewHolder,
          @NonNull ViewHolder viewHolder1) {
        return false;
      }

      @Override
      public void onSwiped(@NonNull ViewHolder viewHolder, int i) {

        final int swipedPosition = viewHolder.getAdapterPosition();
        final VironitStudent student = adapter.getVironitStudents().get(swipedPosition);
        adapter.deleteStudent(swipedPosition);

        final Snackbar snackbarDel = Snackbar
            .make((findViewById(R.id.mainLayout)), "Deleted", Snackbar.LENGTH_LONG);
        snackbarDel.setAction("Return item", new OnClickListener() {
          @Override
          public void onClick(View v) {
            adapter.returnStudentAfterDeleting(swipedPosition);

            vironitStudentsDao.insert(student);

          }
        });
        snackbarDel.setActionTextColor(Color.parseColor("#fa3e3e"));
        snackbarDel.show();

        vironitStudentsDao.deleteById(student.getId());


      }

    };
    ItemTouchHelper mItemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
    mItemTouchHelper.attachToRecyclerView(recyclerView);

  }


  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu, menu);
    MenuItem addStudent = menu.findItem(R.id.addAddNewStudent);
    addStudent.setVisible(false);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();

    if (id == R.id.settings) {
      Intent startSettingsActivityFragment = new Intent(this, FragmentSettingsActivity.class);
      startActivity(startSettingsActivityFragment);
    }

    if (id == R.id.sendAsFile) {

      DialogEmailFragment sendDialog = new DialogEmailFragment();

      sendDialog.show(getSupportFragmentManager(), "sendDialog");

    }

    if (id == R.id.threadProcess) {

      Intent startThreadProcessActivity = new Intent(this, ThreadProcess.class);
      startActivity(startThreadProcessActivity);

    }

    return super.onOptionsItemSelected(item);
  }

  private void regBroadcastAndSnack() {
    mCheckBroadcast.setSnackBroadcast(new snackBroadcast() {
      @Override
      public void setSnackOnActivity(String wifiStatus) {
        final Snackbar snackbar = Snackbar
            .make((findViewById(R.id.mainLayout)), wifiStatus, Snackbar.LENGTH_SHORT);
        snackbar.setAction("close", new OnClickListener() {
          @Override
          public void onClick(View v) {
            snackbar.dismiss();
          }
        });
        snackbar.setActionTextColor(Color.parseColor("#fa3e3e"));
        snackbar.show();
      }
    });

    registerReceiver(mCheckBroadcast,
        new IntentFilter(WifiManager.SUPPLICANT_CONNECTION_CHANGE_ACTION));
  }
}

